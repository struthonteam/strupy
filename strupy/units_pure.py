'''
--------------------------------------------------------------------------
Copyright (C) 2015-2020 Lukasz Laba <lukaszlaba@gmail.com>

This file is part of StruPy.
StruPy structural engineering design Python package.
https://bitbucket.org/struthonteam/strupy

StruPy is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

StruPy is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with StruPy; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
--------------------------------------------------------------------------

File changes:
- python3 compatibility checked
'''

#----fast SI unit alternative list used in StruPy pakage
m  = 1.0E0
cm = 1.0E-2
mm = 1.0E-3
kg = 1.0E0

m2 = 1.0E0
cm2 = 1.0E-4
mm2 = 1.0E-6

m3  = 1.0E0
cm3 = 1.0E-6
mm3 = 1.0E-9

m4 = 1.0E0
cm4 = 1.0E-8
mm4 = 1.0E-12

N = 1.0E0
kN = 1.0E3

Nm = 1.0E0
kNm = 1.0E3

J = 1.0E0

Pa = 1.0E0
kPa = 1.0E3
MPa = 1.0E6
GPa = 1.0E9

# Test if main
if __name__ == '__main__':
    pass